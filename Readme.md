INSTALLATION

git clone https://github.com/owid/etl.git

cd etl

make test (make: ***  Aucune règle pour fabriquer la cible « vendor/*/* », nécessaire pour « .venv ». Arrêt.)

make test (12 passed, 3 warnings)

Pour moi, il est nécessaire de faire 2 make test.


Après avoir entrer ces 4 commandes, soit vous avez une erreur car vous n'avez pas poetry sur votre machine,
soit ce n'est pas la bonne version de python qui est installer, soit il manque des packages, soit tout va bien.



-installer poetry

le lien qui m'a aidé à installer poetry sur ubuntu: https://python-poetry.org/docs/

Pour installer poetry, il faut faire:

curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
//cela a pris 3 minutes pour installer poetry

attention si vous ouvrez un autre terminal et que vous entrez la commande
poetry --version
vous verrez le message poetry : commande introuvable apparaitre.
Ne vous en faites pas, si vous entrez la commande
source $HOME/.poetry/env
et que vous refaites ensuite un poetry --version, il n'y aura plus d'erreur, vous verrez la version actuel de poetry installer.



-installer python 3.9

le lien qui m'a aidé à installer python 3.9 sur ubuntu: https://linuxize.com/post/how-to-install-python-3-9-on-ubuntu-20-04/

sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.9
python3.9 --version



-erreur package virtualenv et MySQLdb

pour virtualenv

le lien qui m'a aidé à résoudre le problème: https://stackoverflow.com/questions/63491221/modulenotfounderror-no-module-named-virtualenv-seed-embed-via-app-data-when-i


si vous avez l'erreur

ModuleNotFoundError

  No module named 'virtualenv.seed.via_app_data'

sudo apt remove --purge python3-virtualenv

pour MySQLdb


autre lien pour l'autre package: https://stackoverflow.com/questions/454854/no-module-named-mysqldb

sudo apt-get install -y python3-mysqldb






PREMIER PAS

Pour savoir toutes les commandes de l'etl, faites make help.
Pour l'instant je vois les commandes:

make etl       Fetch data and run all transformations

make lab       Start a Jupyter Lab server

make test      Run all linting and unit tests

make publish   Publish the generated catalog to S3

make grapher   Publish supported datasets to Grapher

make dot   	Build a visual graph of the dependencies

make watch     Run all tests, watching for changes

make clean     Delete all non-reference data in the data/ folder

make clobber   Delete non-reference data and .venv


Ce que j'ai quand je lance les commandes:

make etl   (BadZipFile: File is not a zip file)   

make lab      (une page s'ouvre dans le navigateur)

make test  (12 passed, 3 warnings)    

make publish  (BadZipFile: File is not a zip file)  

make grapher   (ERROR: No grapher user id has been set in the environment.
       Did you configure the MySQL connection in .env?
)

make dot   	( /bin/sh: 1: dot: not found )

make watch   (ne se termine jamais)  

make clean     (supprime le dossier meadow créer par make etl du dossier data)

make clobber   (supprime le dossier meadow créer par make etl du dossier data,
                supprime .venv(fichier cachés) du dossier etl(à ne pas confondre avec le dossier etl qui est dans le dossier etl)
                make test doit réinstaller tout les packages)





version python actuellement installés: python 3.9.12


TENTATIVE RESOLUTION PROBLEME

En regardant le readme, je vois qu'il faut Levenshtein, je l'ai donc installé grace au lien suivant:
https://dfordatascience.wordpress.com/2020/12/04/how-to-install-python-levenshtein-on-linux-ubuntu/

J'ai eu le message suivant.

Successfully installed python-Levenshtein-0.12.2

Malheureusement, cela n'a pas résolus le problème.

Il faut aussi des dev headers, installé grace au lien suivant:

https://stackoverflow.com/questions/8282231/i-have-python-on-my-ubuntu-system-but-gcc-cant-find-python-h


Du coup j'ai ouvert un ticket. https://github.com/owid/etl/issues/176
