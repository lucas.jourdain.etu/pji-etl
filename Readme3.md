Pour traiter le fichier csv de la sncf:
- modifier les droits d'accès de .venv/bin/activate, se donner le droit d'éxécuter le fichier comme un programme
- ouvrir un terminal et entrer la commande: source .venv/bin/activate
- installer pycurl (pip install pycurl)
- mettre le fichier dag_test.yml dans dag_files
- modifier le fichier dag.yml en lui rajoutant à la fin du fichier: - dag_files/dag_test.yml
- entrer la commande: .venv/bin/etl data://sncf

petite précision, la commande marchera même si l'environnement n'est plus activée


CLEAN, HARMONIZE AND REMIX

Pour cette partie, il y a juste plusieurs chose à faire.

- Rajouter un fichier .py dans etl/steps/data/
- Dans ce fichier construire la fonction run qu'il vous faut(plusieurs exemples sont à votre disposition)
- rajouter dans votre fichier .yml la data step qui correspond
- entrer la commande spécifique qui permet de lancer juste la step qui correspond au fichier .py que vous venez de mettre ou faire make etl


PUBLISH DATA

Cette partie reste à faire.

L'issue suivante a été ouverte pour avoir plus d'informations à ce sujet: https://github.com/owid/etl/issues/199

Un lien pour installer et utiliser les buckets de DigitalOcean Spaces: https://docs.digitalocean.com/products/spaces/resources/s3-sdk-examples/
