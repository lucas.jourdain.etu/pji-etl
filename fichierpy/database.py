import pandas as pd


df = pd.read_csv('tableau1.csv') 

from owid.catalog import Dataset, Table

# %% Clean it up

#s'il y a des données à supprimer de la base de données il faut le faire avant d'appeler la fonction run


# %% Create dataset in the `run` function using module-level variables


def run(dest_dir: str) -> None:
    ds = Dataset.create_empty(dest_dir)
    ds.metadata.short_name = "test"

    # use module-level variables
    t = Table(df.reset_index(drop=True))
    t.metadata.short_name = "test"

    ds.add(t)
    ds.save()
