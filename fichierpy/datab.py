import pandas as pd
from owid.catalog import Dataset, Table

data1 = pd.read_csv('tableau1.csv') 

data2 = pd.read_csv('tableau2.csv') 

df = pd.merge(data1, data2,  
                   on='identifiant',  
                   how='outer') 


# pour vous aider à faire jointure entre 2 fichiers csv
#https://fr.acervolima.com/comment-fusionner-deux-fichiers-csv-par-colonne-specifique-en-utilisant-pandas-en-python/

# %% Clean it up

#s'il y a des données à supprimer de la base de données il faut le faire avant d'appeler la fonction run

# %% Create dataset in the `run` function using module-level variables


def run(dest_dir: str) -> None:
    ds = Dataset.create_empty(dest_dir)
    ds.metadata.short_name = "test"

    # use module-level variables
    t = Table(df.reset_index(drop=True))
    t.metadata.short_name = "test"

    ds.add(t)
    ds.save()
