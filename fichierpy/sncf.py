from owid.catalog import Dataset, Table
import pycurl
import pandas as pd

file_name = 'sncf5.csv'

url4 = "https://ressources.data.sncf.com/explore/dataset/referentiel-gares-voyageurs/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B"

with open(file_name, 'wb') as f:
    cl = pycurl.Curl()
    cl.setopt(cl.URL, url4)
    cl.setopt(cl.WRITEDATA, f)
    cl.perform()
    cl.close()

#nomColonne = ["code_plate_forme", "code_gare", "code_uic", "date_fin_validite_plateforme", "intitule_plateforme",
#"code_postal", "code_commune", "commune", "code_departement", "departement",
#"longitude", "latitude", "segment_drg", "niveau_de_service", "rg",
#"tvss", "sops", "gare", "intitule_gare", "intitule_fronton_de_gare",
#"gare_drg", "gare_etrangere", "dtg", "region_sncf",
#"unite_gare", "ut", "nbre_plateformes", "tvs", "wgs_84", "wgs_84_bis"] 

#names = nomColonne)

def run(dest_dir: str) -> None:
    ds = Dataset.create_empty(dest_dir)
    ds.metadata.short_name = "sncf"

    df = pd.read_csv(file_name, sep = ";")

    df.columns = df.columns.str.replace(' ', '_')# on remplace les espaces par des _
    df.columns = df.columns.str.replace('-', '_')# on remplace les - par des _
    df.columns = df.columns.str.replace('é', 'e')# on remplace les é par des e
    df.columns = df.columns.str.replace('è', 'e')# on remplace les è par des e

    l = list(df)# liste des noms d'origine
    l2 = list() # liste des nouveaux noms, actuellement vide

    tailleliste = len(l)

    for i in l:
        l2.append(i.lower())# on a maintenant les nouveaux noms de colonnes, changeons maintenant ceux du dataframe par les nouveaux

    df.columns = l2 #on change les noms de colonnes du dataframe 

    t = Table(df.reset_index(drop=True))
    t.metadata.short_name = "sncf"

    ds.add(t)
    ds.save()
